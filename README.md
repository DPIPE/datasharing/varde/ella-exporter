# ELLA-exporter

Code to prepare variant classification data from ELLA software for inclusion in Varde.

Steps in main script vardedb.sh:
1) Generates variants from ella-database and writes to a vcf-file
2) Generated vcf-file is parsed to VARDE-db-style. I.e, parses ACGM-criteria to legal output
3) Runs petimeter on vcf-file-ready-for-pedant.

# SETUP: 
 - Clone folder VARDE-DB to proper area. (i.e /ella-prod/data/export/)
 - Edit vardedb-sourceme with data paths fitting ELLA-paths to your TSD-project

# RUN: 
./vardedb.sh 

# TEST:
Only tested on real data - not tested on simulated data. 

# TODO: 
 - add test set 
 - docker







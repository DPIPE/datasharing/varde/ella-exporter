#!/bin/bash -ue
set -o pipefail

THISDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
pushd $THISDIR > /dev/null

tmp_file=$(mktemp)
echo "Generating VCF with all SNV alleleassessments in ELLA"


# Generate vcf-file
psql ${DB_URL} -A -t -F $'\t' --no-align -f export-classifications-to-vcf.sql \
 | sort -k1,1V -k2,2n \
 | cat ${HEADER} - \
 > $tmp_file
echo "VCF generated, with $(grep -v '^#' ${tmp_file} | wc -l) variants"

MD5SUM_GENERATED=$(md5sum $tmp_file | cut -d' ' -f1)

# Copy to anno/sensitive-db/ella-classifications/ if there are any changes, and update symlink
SENSITIVE_DB=/sensitive-db/

now=$(date +"%Y%m%d_%H%M%S")
LINK_NAME=ella-classifications.vcf
TARGET_FILE=ella-classifications/ella-classifications-${now}.vcf



pushd "$SENSITIVE_DB" >/dev/null

MD5SUM_EXISTING=$(md5sum ${LINK_NAME} | cut -d' ' -f1 || true)
if [[ "${MD5SUM_EXISTING}" == "${MD5SUM_GENERATED}" ]]; then
        echo "No changes to $LINK_NAME. Skipping..."
        rm $tmp_file
        exit
fi



cat $tmp_file > $TARGET_FILE
rm $tmp_file
ln -sf $TARGET_FILE $LINK_NAME
chmod g+rw $TARGET_FILE $LINK_NAME
echo "Updated: $(ls -l $LINK_NAME)"



popd >/dev/null





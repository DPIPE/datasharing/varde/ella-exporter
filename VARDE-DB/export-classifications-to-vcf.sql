WITH assessment_ids_acmg_codes AS (
        SELECT 
                id, 
                array_agg(code) AS acmg_codes
        FROM (                               
                SELECT 
                        id, 
                        replace((jsonb_array_elements(evaluation->'acmg'->'included')->'code')::text, '"', '') AS code 
                FROM alleleassessment 
                WHERE date_superceeded IS NULL                                                                                        
                ) AS foo 
        GROUP BY 1                            
),                       
allele_ids_assessment_ids AS (
        SELECT
                id,
                allele_id ,
                classification,
		date_created
        FROM alleleassessment
        WHERE date_superceeded IS NULL
),

alleleid_pubmedid AS (
	SELECT
		allele_id,
		array_agg(pubmed_id) as pubmed_id
		FROM (
		     SELECT DISTINCT
		     	    r.pubmed_id,
			    ra.reference_id,
			    ra.allele_id
		     FROM reference r,
		     	  referenceassessment ra,
			  alleleassessment ae
		     WHERE ae.allele_id=ra.allele_id and ra.reference_id=r.id and ae.date_superceeded IS NULL ORDER BY ra.allele_id) AS foo GROUP BY 1
)


SELECT DISTINCT  
        chromosome, 
        vcf_pos, 
        '.',                                                                         
        vcf_ref as ref, 
        vcf_alt as alt, 
        '.' as qual, 
        '.' as filter,
        'CLASS=' || classification || ';ACMG=' || coalesce(array_to_string(acmg_codes, ','), '.') || ';YEAR=' || to_char(date_created,'YYYY') || ';SVLEN=' || length || ';PUBMED=' || coalesce(array_to_string(pubmed_id, ','),'.') as info
FROM allele                                                                                                    
JOIN allele_ids_assessment_ids AS aa
ON allele.id=aa.allele_id
LEFT OUTER JOIN assessment_ids_acmg_codes AS aac
ON aac.id=aa.id
LEFT OUTER JOIN alleleid_pubmedid AS ai_pm
ON aa.allele_id=ai_pm.allele_id
WHERE caller_type='snv';

import os,sys

######################################################################
## translates every legal ACMG-criteria from EllA-style to VARDE-style
######################################################################
def defineACMG():
    acmg={}
    acmg["."] = ""

    # Pathogenic very strong
    acmg["PVS1"]="PVS1"
    acmg["PSxPVS1"]="PVS1_S"
    acmg["PMxPVS1"]="PVS1_M"
    acmg["PPxS1"]="PVS1_P"

    # Pathogenic strong
    acmg["PS1"]="PS1"
    acmg["PVSxPS1"]="PS1_VS"
    acmg["PMxPS1"]="PS1_M"
    acmg["PPxPS1"]="PS1_P"
    acmg["PS2"]="PS2"
    acmg["PVSxPS2_VS"]="PS2_VS"
    acmg["PMxPS2"]="PS2_M"
    acmg["PPxPS2"]="PS2_P"
    acmg["PS3"]="PS3"
    acmg["PVSxPS3"]="PS3_VS"
    acmg["PMxPS3"]="PS3_M"
    acmg["PPxPS3"]="PS3_P"
    acmg["PS4"]="PS4"
    acmg["PVSxPS4"]="PS4_VS"
    acmg["PMxPS4"]="PS4_M"
    acmg["PPxPS4"]="PS4_P"

    # Pathogenic moderate
    acmg["PM1"]="PM1"
    acmg["PM1_VS"]="PM1_VS"
    acmg["PSxPM1"]="PM1_S"
    acmg["PPxPM1"]="PM1_P"
    acmg["PM2"]="PM2"
    acmg["PVSxPM2"]="PM2_VS"
    acmg["PSxPM2"]="PM2_S"
    acmg["PPxPM2"]="PM2_P"
    acmg["PM3"]="PM3"
    acmg["PVSxPM3"]="PM3_VS"
    acmg["PSxPM3"]="PM3_S"
    acmg["PPxPM3"]="PM3_P"
    acmg["PM4"]="PM4"
    acmg["PVSxPM4"]="PM4_VS"
    acmg["PSxPM4"]="PM4_S"
    acmg["PPxPM4"]="PM4_P"
    acmg["PM5"]="PM5"
    acmg["PVSxPM5"]="PM5_VS"
    acmg["PSxPM5"]="PM5_S"
    acmg["PPxPM5"]="PM5_P"
    acmg["PM6"]="PM6"
    acmg["PVSxPM6"]="PM6_VS"
    acmg["PSxPM6"]="PM6_S"
    acmg["PPxPM6"]="PM6_P"

    # Pathogenic supportive
    acmg["PP1"]="PP1"
    acmg["PVSxPP1"]="PP1_VS"
    acmg["PSxPP1"]="PP1_S"
    acmg["PMxPP1"]="PP1_M"
    acmg["PP2"]="PP2"
    acmg["PVSxPP2"]="PP2_VS"
    acmg["PSxPP2"]="PP2_S"
    acmg["PMxPP2"]="PP2_M"
    acmg["PP3"]="PP3"
    acmg["PVSxPP3"]="PP3_VS"
    acmg["PSxPP3"]="PP3_S"
    acmg["PMxPP3"]="PP3_M"
    acmg["PP4"]="PP4"
    acmg["PVSxPP4"]="PP4_VS"
    acmg["PSxPP4"]="PP4_S"
    acmg["PMxPP4"]="PP4_M"
    acmg["PP5"]="PP5"
    acmg["PVSxPP5"]="PP5_VS"
    acmg["PSxPP5"]="PP5_S"
    acmg["PMxPP5"]="PP5_M"

    # Benign stand-alone
    acmg["BA1"]="BA1"
    acmg["BSxBA1"]="BA1_S"
    acmg["BPxBA1"]="BA1_P"

    # Benign strong
    acmg["BS1"]="BS1"
    acmg["BAxBS1"]="BS1_A"
    acmg["BPxBS1"]="BS1_P"
    acmg["BS2"]="BS2"
    acmg["BAxBS2"]="BS2_A"
    acmg["BPxBS2"]="BS2_P"
    acmg["BS3"]="BS3"
    acmg["BAxBS3"]="BS3_A"
    acmg["BPxBS3"]="BS3_P"
    acmg["BS4"]="BS4"
    acmg["BAxBS4"]="BS4_A"
    acmg["BPxBS4"]="BS4_P"

    # Benign supportive
    acmg["BP1"]="BP1"
    acmg["BAxBP1"]="BP1_A"
    acmg["BSxBP1"]="BP1_S"
    acmg["BP2"]="BP2"
    acmg["BAxBP2"]="BP2_A"
    acmg["BSxBP2"]="BP2_S"
    acmg["BP3"]="BP3"
    acmg["BAxBP3"]="BP3_A"
    acmg["BSxBP3"]="BP3_S"
    acmg["BP4"]="BP4"
    acmg["BAxBP4"]="BP4_A"
    acmg["BSxBP4"]="BP4_S"
    acmg["BP5"]="BP5"
    acmg["BAxBP5"]="BP5_A"
    acmg["BSxBP5"]="BP5_S"
    acmg["BP6"]="BP6"
    acmg["BAxBP6"]="BP6_A"
    acmg["BSxBP6"]="BP6_S"
    acmg["BP7"]="BP7"
    acmg["BAxBP7"]="BP7_A"
    acmg["BSxBP7"]="BP7_S"

    # NON-ACMG
    acmg["NON-ACMG"]="NON-ACMG"
    return acmg



######################################################################
## fitVCFtoPedant:  
# - converts EllA-style-ACMG to VARDE-style-ACMG
# - SVLEN: NOT USED FOR NOW: (Rewrites SVLEN; only used for length>50 bp)
# - PUBMED: Used as is
# - YEAR: Used as is 
# - INPUT file: VCF generated by Ella-exporter
# - OUTPUT file: File ready to be validated using 'pedimeter' (pedant) - BASENAME_pedant.vcf
######################################################################
def fitVCFtoPedant(infilename,outfilename):

    infile = open(infilename,'r')
    outfile = open(outfilename,'w')

    lines = infile.readlines()
    for l in lines:
        if l.find("#")!=-1:
            outfile.write(l.strip()+os.linesep)
            continue
        
        indexCLASS = l.find("CLASS")
        CLASS = l[indexCLASS+6:].split(";")[0]
    
        indexACMG = l.find("ACMG")
        ACMG = l[indexACMG+5:].split(";")[0].split(",")

        indexYEAR = l.find("YEAR")
        YEAR = l[indexYEAR+5:].split(";")[0]

        indexSVLEN = l.find("SVLEN")
        SVLEN = int(l[indexSVLEN+6:].split(";")[0])
        if SVLEN>=SVLENSTARTVALUE:
            SVLEN = str(SVLEN)
        else:
            SVLEN="."
        indexPUBMED = l.find("PUBMED")
        PUBMED = l[indexPUBMED+7:].split(";")[0].strip()

        acmg_string = ''
        for c in ACMG:
            if c not in acmg:
                continue
                #print(cols2,c,c)
            else:
                acmg_string += acmg[c]+","
                #print(cols2,c,acmg[c])    
        if acmg_string!="":
            acmg_string = acmg_string[0:-1]
        
        VARIANTPART = l[0:indexCLASS]
        INFO = ""
        INFO +="CLASS="+CLASS

        if acmg_string!='':
            INFO +=";ACMG="+acmg_string

        INFO +=";YEAR="+YEAR
        if SVLEN!=".":
            INFO +=";SVLEN="+str(SVLEN)
        
        if PUBMED!=".":
            INFO +=";PUBMED="+PUBMED

        newline = VARIANTPART+INFO
        outfile.write(newline+os.linesep)



# RUN: python makeVCF_for_pedant.py [ella-classifications.vcf] [outname_for_pedant.vcf]

SVLENSTARTVALUE = 5000
infilename = sys.argv[1]
outfilename = sys.argv[2]
acmg = defineACMG()
fitVCFtoPedant(infilename,outfilename)


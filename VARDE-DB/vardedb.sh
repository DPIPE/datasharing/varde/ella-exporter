#!/bin/bash -ue


# source in to run exporter
source vardedb-sourceme

# export data from ella-database
${CMD} /data/export/VARDE-DB/export-classifications-to-vcf.sh


# Do some changes to exported vcf from ella. Petimeter-fit
ellavcf="sensitive-db/ella-classifications.vcf"
now=`ls -l sensitive-db/ella-classifications.vcf|cut -d'/' -f3|cut -d'.' -f1|cut -d'-' -f3|cut -d'_' -f1`

pedantvcf="sensitive-db-pedant/STOAMG-varde-${now}.vcf"
python makeVCF_for_pedant.py $ellavcf $pedantvcf


# run petimeter
./pedant/petimeter validate --debug $pedantvcf

gpg -u STOAMG -s -e -r vardedb $pedantvcf > $pedantvcf.gpg
